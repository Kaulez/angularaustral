import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioDestinoComponent } from './formulario-destino.component';

describe('FormularioDestinoComponent', () => {
  let component: FormularioDestinoComponent;
  let fixture: ComponentFixture<FormularioDestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioDestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioDestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
