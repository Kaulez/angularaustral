import { formatCurrency } from '@angular/common';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter,debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import{Destino} from './../Models/Destino-Viaje.models';
import{ajax, AjaxResponse} from 'rxjs/ajax';
@Component({
  selector: 'app-formulario-destino',
  templateUrl: './formulario-destino.component.html',
  styleUrls: ['./formulario-destino.component.css']
})
export class FormularioDestinoComponent implements OnInit {
@Output() onItemAdded:EventEmitter<Destino>;
minLength=3;
SearchResult:string[];
fg:FormGroup;
  constructor(fb:FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg=fb.group({
      nombre:['',Validators.compose([
        Validators.required,
        this.validadorNombre,
        this.validadorParametrizable(this.minLength),
      ])],
      url:['',Validators.required]
    });
    this.fg.valueChanges.subscribe((form:any)=>{
      console.log('cambio en el formulario ', form);
    });
   }

  ngOnInit(): void {
    let elemNombre=<HTMLInputElement>document.getElementById("nombre");
    fromEvent(elemNombre,'input')
    .pipe(map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),
    filter(text=>text.length>4),
    debounceTime(200),
    distinctUntilChanged(),
    switchMap(()=>ajax('assets/datos.json'))
    ).subscribe(AjaxResponse=>{
      this.SearchResult=AjaxResponse.response;  
    });
  }
  guardar(nombre:string,url:string){
    const d=new Destino(nombre,url);
    this.onItemAdded.emit(d);
    return false;
  }
  validadorNombre(nombre:FormControl):{[s:string]:boolean}{
    const longitud= nombre.value.toString().trim().length;
    if((longitud>0)&&(longitud<5)){
      return {invalidNombre:true};
    }else{
      return null;
    }  
  }
  validadorParametrizable(minlength:number):ValidatorFn{
    return (control:FormControl):{[s:string]:boolean}|null=>{
      const longitud= control.value.toString().trim().length;
        if((longitud>0)&&(longitud<minlength)){
          return {invalidNombreParametrizable:true};
        }
    }

  }
}
