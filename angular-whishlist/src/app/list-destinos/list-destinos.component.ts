import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoDestinoAction, NuevoDestionAction } from '../Models/destinos-viajes-state-models';

import{Destino} from './../Models/Destino-Viaje.models';
import{DestinosApiClient} from './../Models/destinos-api-client.model';

@Component({
  selector: 'app-list-destinos',
  templateUrl: './list-destinos.component.html',
  styleUrls: ['./list-destinos.component.css']
})
export class ListDestinosComponent implements OnInit {
@Output() onItemAdded:EventEmitter<Destino>;
@Input() Destinos: Destino[];
Update :string[];
all;

  constructor(public destinosApiClient:DestinosApiClient,private store:Store<AppState>) {
    this.Destinos=[];
    this.Update=[];
    this.store.select(state=>state.destinos.favorito).subscribe(d=>{
      if(d!=null){
        this.Update.push('Se eligio como favorito a'+d.nombre);
      }
    })
   // this.onItemAdded= new EventEmitter();
   store.select(state=>state.destinos.items).subscribe(items=>this.all=items);
   }
  ngOnInit(): void {
  }
  agregar(destino:Destino){
    this.destinosApiClient.add(destino);
 //   this.onItemAdded.emit(destino);
  //  this.store.dispatch(new NuevoDestionAction(destino));
  }
  elegir(d:Destino){
    this.destinosApiClient.elegir(d);
    //this.store.dispatch(new ElegidoFavoritoDestinoAction(d));
  }
}
