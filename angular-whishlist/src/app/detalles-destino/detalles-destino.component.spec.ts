import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesDestinoComponent } from './detalles-destino.component';

describe('DetallesDestinoComponent', () => {
  let component: DetallesDestinoComponent;
  let fixture: ComponentFixture<DetallesDestinoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesDestinoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesDestinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
