import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListDestinosComponent } from './list-destinos/list-destinos.component';
import { DetallesDestinoComponent } from './detalles-destino/detalles-destino.component';
import { FormularioDestinoComponent } from './formulario-destino/formulario-destino.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{DestinosApiClient} from './Models/destinos-api-client.model';
import { DestinosViajeState, reducerDestinosViajes,initalizeDestinoViajeState,DestinoViajesEffects } from './Models/destinos-viajes-state-models';
import { ActionReducerMap,StoreModule as NgRxStoreModule} from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
const routes: Routes=[
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path:'home',component:ListDestinosComponent},
  {path:'detalles',component:DetallesDestinoComponent}];
  //redux init
  export interface AppState{
    destinos: DestinosViajeState;
  }
  const reducers:ActionReducerMap<AppState>={
    destinos:reducerDestinosViajes
  }
  let reducersInitialstate={
    destinos:initalizeDestinoViajeState()
  }
  //redux fin init
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListDestinosComponent,
    DetallesDestinoComponent,
    FormularioDestinoComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(routes),
    FormsModule,ReactiveFormsModule,NgRxStoreModule.forRoot(reducers,{initialState:reducersInitialstate,runtimeChecks: {
      strictStateImmutability: false,
      strictActionImmutability: false,
    }}),
    EffectsModule.forRoot([DestinoViajesEffects])
  ],
  providers: [DestinosApiClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
