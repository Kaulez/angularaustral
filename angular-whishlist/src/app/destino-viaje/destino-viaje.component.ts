import { Component, OnInit,HostBinding,Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Destino } from '../Models/Destino-Viaje.models';
import { VoteDownAction, VoteUpAction ,VoteResetAction} from '../Models/destinos-viajes-state-models';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})

export class DestinoViajeComponent implements OnInit {
  @HostBinding('attr.class')cssClass="col-md-4";
  @Input() Destino: Destino;
  @Input() Posicion:number;
  @Output() Clicked:EventEmitter<Destino>;
  constructor(private store:Store<AppState>) {
    this.Clicked= new EventEmitter();

  }  
  ngOnInit(): void {
  }
 ir():boolean{
  this.Clicked.emit(this.Destino);
  return false;
 }
 voteup(){
   this.store.dispatch(new VoteUpAction(this.Destino));
   return false;
 }
 voteDown(){
  this.store.dispatch(new VoteDownAction(this.Destino));
   return false;
 }
 resetVotes(){
   this.store.dispatch(new VoteResetAction(this.Destino));
 }

}
