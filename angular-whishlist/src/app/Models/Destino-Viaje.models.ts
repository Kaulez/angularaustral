export class Destino{
    selected:boolean;
    Servicios:String[];
    constructor(public nombre:string, public url:string,public Votes:number=0){
        this.Servicios=['Desayuno','Pasteles']; 
    }
    
    IsSelected():boolean{
        return this.selected;
    }
    SetSelected(Set:boolean){
        this.selected=Set;
    }
    VoteUp(){
        this.Votes++;
    }
    VoteDown(){
        this.Votes--;
    }
    VoteReset(){
        this.Votes=0;
    }
}