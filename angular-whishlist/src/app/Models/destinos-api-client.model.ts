import { Destino } from './Destino-Viaje.models';
import { BehaviorSubject, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoDestinoAction,NuevoDestionAction } from './destinos-viajes-state-models';
import { Injectable } from '@angular/core';
@Injectable()
export class DestinosApiClient {
	//destinos:Destino[];
	//current:Subject<Destino>= new BehaviorSubject<Destino>(null);//Este es el observable
	constructor(private store:Store<AppState>) {
     //  this.destinos = [];
	}
	add(d:Destino){
		this.store.dispatch(new NuevoDestionAction(d));
	//  this.destinos.push(d);
	}
	
	elegir(d:Destino){
		this.store.dispatch(new ElegidoFavoritoDestinoAction(d));
	//	this.destinos.forEach(x=>x.SetSelected(false));
	//	d.SetSelected(true);
	//	this.current.next(d);
	}
	/*suscribeOnChange(fn){
		this.current.subscribe(fn);//para que los demas objetos se suscriban al observable
	}*/
}