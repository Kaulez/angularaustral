import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Destino } from './Destino-Viaje.models';
 //Estado
 export interface DestinosViajeState{
     items: Destino[];
     loading:boolean;
     favorito:Destino;
 }
 export const initalizeDestinoViajeState=function(){
    return{
        items:[],
        loading:false,
        favorito:null,
    }
 }
 //Actions
 export enum DestinoViajeActionTypes{
     NUEVO_DESTINO ='[Destino] Nuevo',
     ELEGIDO_FAVORITO='[Destino] Favorito',
     VOTE_UP='[Destino] Vote Up',
     VOTE_DOWN='[Destino] Vote Down',
     VOTE_RESET="[Destino] Vote Reset",
 }
 export class NuevoDestionAction implements Action{
     type: DestinoViajeActionTypes.NUEVO_DESTINO;
     constructor(public destino:Destino){};
 }
 export class ElegidoFavoritoDestinoAction implements Action{
     type: DestinoViajeActionTypes.ELEGIDO_FAVORITO;
     constructor(public destino:Destino){}
 }
 export class VoteUpAction implements Action{
    type: DestinoViajeActionTypes.VOTE_UP;
    constructor(public destino:Destino){}
}
export class VoteDownAction implements Action{
    type: DestinoViajeActionTypes.VOTE_DOWN;
    constructor(public destino:Destino){}
}
export class VoteResetAction implements Action{
    type: DestinoViajeActionTypes.VOTE_RESET;
    constructor(public destino:Destino){}
}
 export type DestinoViajesAction=NuevoDestionAction|ElegidoFavoritoDestinoAction|VoteUpAction|VoteDownAction|VoteResetAction;
 //Reducers
 export function reducerDestinosViajes(state:DestinosViajeState, action:DestinoViajesAction):DestinosViajeState{
    switch(action.type){
        case DestinoViajeActionTypes.NUEVO_DESTINO:{
            return{
                ...state,
                items:[...state.items,(action as NuevoDestionAction).destino]
            };
        }
        case DestinoViajeActionTypes.ELEGIDO_FAVORITO:{
            state.items.forEach(x=>x.SetSelected(false));
            let fav:Destino=(action as ElegidoFavoritoDestinoAction).destino;
            fav.SetSelected(true);
            return{      
                ...state,
                favorito:fav,         
            };
        }
        case DestinoViajeActionTypes.VOTE_UP:{          
            const d:Destino=(action as VoteUpAction).destino;
            d.VoteUp();
            return{      
                ...state,        
            };
        }
        case DestinoViajeActionTypes.VOTE_DOWN:{          
            const d:Destino=(action as VoteDownAction).destino;
            d.VoteDown();
            return{      
                ...state,        
            };
        }
        case DestinoViajeActionTypes.VOTE_RESET:{          
            const d:Destino=(action as VoteResetAction).destino;
            d.VoteReset();
            return{      
                ...state,        
            };
        }
    }
    return state;
 }
 //Efects
@Injectable()
export class DestinoViajesEffects{
    @Effect()
    nuevoAgregado$:Observable<Action>=this.actions$.pipe(
        ofType(DestinoViajeActionTypes.NUEVO_DESTINO),
        map((action:NuevoDestionAction)=> new ElegidoFavoritoDestinoAction(action.destino)),
    );
    constructor(private actions$: Actions){};
}